import json
import configparser

from pathlib import Path

from modules.query import DatabaseObject
from modules.query import QueryObject
from modules.logger import *

try:
    create_log()
    # Get Config Options
    config = configparser.ConfigParser()
    config.read(Path("settings.ini"))
    data_file = config["data"]["all"]

    # Init DB Connection
    db = DatabaseObject(
        u=config["db"]["username"],
        p=config["db"]["con_pw"],
        c_ip=config["db"]["con_ip"],
        c_port=config["db"]["con_port"],
        c_sid=config["db"]["con_sid"],
    )

    # Connect to DB -> cursor
    session_cursor = db.connect()

    # Load Data
    session_data_file = open(Path(data_file), "r")
    session_query_data = json.loads(session_data_file.read())
    session_data_file.close()

    #
    for each_data in session_query_data:
        new_q = QueryObject(each_data, session_cursor)
        # new_q.start()

    db.close()

except Exception as e:
    print(e)
