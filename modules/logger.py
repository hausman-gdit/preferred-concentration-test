from datetime import datetime
from pathlib import Path

datetime_format = "%m-%d-%y"
folder_format = "%m_%d_%y"


def create_log():
    session_start = datetime.now().strftime(datetime_format)
    global log_fname
    log_fname = session_start.replace(":", "") + "_log.txt"
    lf_p = Path(log_fname)
    try:
        lf_p.touch(mode=438, exist_ok=False)
        print(f"Creating file: {lf_p} ...")
        write_log(msg=f"Session Start:{session_start} @ {lf_p}", kind="info")

    except FileExistsError:
        write_log(msg=f'Skipping ... File "{lf_p}" exists', kind="warn")


def write_log(**kwargs):
    msg = kwargs["msg"]
    msg_kind = kwargs["kind"].upper()
    with open(log_fname, "a") as l_open:
        t_now = datetime.now().strftime(f"{datetime_format} - ")
        print(f"{t_now}{msg_kind} - {msg}")
        l_open.write(f"{t_now}{msg_kind} - {msg}\n")


def create_summary():
    session_start = datetime.now().strftime(folder_format)
    global summary_fname
    summary_fname = "test_" + session_start + ".txt"
    cs_p = Path(summary_fname)
    try:
        cs_p.touch(mode=438, exist_ok=False)
        print(f"Creating file: {cs_p} ...")
        write_log(msg=f"Session Start:{session_start} @ {cs_p}", kind="info")

    except FileExistsError:
        write_log(msg=f'Skipping ... File "{cs_p}" exists', kind="warn")


def write_summary(**kwargs):
    msg = kwargs["msg"]
    msg_kind = kwargs["kind"].upper()
    with open(summary_fname, "a") as l_open:
        t_now = datetime.now().strftime(f"{datetime_format} - ")
        print(f"{t_now}{msg_kind} - {msg}")
        l_open.write(f"{t_now}{msg_kind} - {msg}\n")
