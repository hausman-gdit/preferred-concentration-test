import csv

from pathlib import Path

from .logger import write_log, write_summary


class FileObject:
    def __init__(self) -> None:
        pass

    def write_results(self, **kwargs):
        d_root = "ecotox_ascii_" + kwargs["root"] + "/"
        self.d_name = d_root + kwargs["d"]
        self.f_name = kwargs["f"]
        self.f_headers = kwargs["h"]
        self.f_delim = kwargs["l"]
        self.results = kwargs["r"]

        self.create_file()
        self.write_file()

    def create_file(self):
        cf_p = Path(self.f_name)
        try:
            cf_p.touch(exist_ok=False)
            write_log(msg=f"Creating file: {cf_p} ...", kind="info")
        except FileExistsError:
            write_log(msg=f'Re-writing File ... "{cf_p}"', kind="warn")

    def write_file(self):
        try:
            if self.results is None:
                pass
            else:
                write_summary(
                    msg=f"{self.f_name} - {len(self.results)}", kind="success"
                )
                wf_p = Path(self.f_name)
                with open(wf_p, "w", newline="") as wf_open:
                    file_writer = csv.writer(wf_open, delimiter=self.f_delim)
                    file_writer.writerow(eh for eh in self.f_headers.split(","))
                    for er in self.results:
                        file_writer.writerow(er)
        except TypeError as e:
            write_log(msg=f"Write File TypeError, {self.f_name}: {e}", kind="error")
            write_summary(
                msg=f"Write File TypeError, {self.f_name}: {e}", kind="failure"
            )

        except Exception as e:
            write_log(msg=f"Write File Exception, {self.f_name}: {e}", kind="error")
            write_summary(msg=f"{self.f_name} did not write : {e}", kind="failure")
