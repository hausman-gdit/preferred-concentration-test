import json
import configparser
import cx_Oracle as oracledb

from pathlib import Path

from .results import FileObject as fo
from .logger import write_log, write_summary


class DatabaseObject:
    def __init__(self, **kwargs):
        # DB Connection Init/Basic Info
        self.username = kwargs["u"]
        self.password = kwargs["p"]
        self.p_add = kwargs["c_ip"]
        self.p_port = kwargs["c_port"]
        self.p_sid = kwargs["c_sid"]

        self.this_dsn = oracledb.makedsn(self.p_add, self.p_port, self.p_sid)

    def connect(self):
        # Connect to the Database
        try:
            self.connection = oracledb.connect(
                user=self.username, password=self.password, dsn=self.this_dsn
            )
            write_log(
                msg=f"Connection Established:{self.username}:****@{self.this_dsn}!",
                kind="info",
            )

            return self.connection.cursor()

        except Exception as e:
            write_log(
                msg=f"Database Connection Exception occured:\n{self.__dict__}\n{e}",
                kind="error",
            )
            write_summary(
                msg=f"Database Connection Exception occured:\n{self.__dict__}\n{e}",
                kind="error",
            )

    def close(self):
        # Close Database Connection
        try:
            self.connection.close()
            write_log(
                msg=f"Connection Closed {self.username}:****@{self.this_dsn}!",
                kind="info",
            )

        except Exception as e:
            write_log(
                msg=f"Connection Failed {self.username}:****@{self.this_dsn}!",
                kind="error",
            )


class QueryObject:
    """Builds & executes query, returns results, passes results off to be written."""

    def __init__(self, data, session_cursor):
        try:
            # Basic Query info
            self.cur = session_cursor
            self.conc = data["in_conc"]
            self.conc_unit = data["in_conc_unit"]
            self.tax_kingdom = data["in_tax_kingdom"]
            self.tax_phylum = data["in_tax_phylum"]
            self.tax_order = data["in_tax_order"]
            self.tax_class = data["in_tax_class"]
            self.tax_family = data["in_tax_family"]
            self.exp_type = data["in_exp_type"]
            self.exp_group = data["in_exp_group"]
            self.test_loc = data["in_test_loc"]
            self.habitat = data["in_habitat"]

            self.multi = self.find_multi()
            print(self.multi)
        except KeyError as e:
            write_log(msg=f"KeyError: {e}", kind="error")

    def find_multi(self):
        # open multiplier -find match
        config = configparser.ConfigParser()
        config.read(Path("settings.ini"))
        multi_file = config["data"]["multi"]
        multi_fields = ["mg_l_mult", "ug_l_mult", "ozacre_mult", "molar_mult"]

        with open(Path(multi_file), "r") as mf:
            session_multi_data = json.loads(mf.read())
            for e_multi in session_multi_data:
                if e_multi["code"] == self.conc_unit:
                    for e_field in multi_fields:
                        if e_multi[e_field] != "":
                            return {e_field: e_multi[e_field]}

    def start(self):
        try:
            if self.group == "execute":
                # Get the results and pass info/results off to be written to a file
                results = self.get_results()
                fo().write_results(
                    d=self.directory,
                    f=self.filename,
                    h=self.headers,
                    l=self.delimiter,
                    r=results,
                    root=self.root_dir,
                )
            else:
                # If the group isn't "execute" then just perform DB operation
                # Handles creating/dropping views for pulling results to be written after
                self.cur.execute(self.query)

        # These should help catch any wonky key,directory,filename,obj vs str things
        except AttributeError as e:
            write_log(msg=f"Start AttributeError {str(e)}", kind="error")
        except TypeError as e:
            write_log(msg=f"Start TypeError {str(e)}", kind="error")
        except Exception as e:
            write_log(msg=f"Start Exception {str(e)}", kind="error")

    def get_results(self):
        # Executes query, returns results, gets a count of rows returned (not with header!)
        try:
            write_log(msg=f"Getting data... {self.filename}", kind="info")
            self.cur.execute(self.query)
            return self.cur.fetchall()
        except Exception as e:
            # Writes failures to session log & session summary
            write_log(msg=f"Query Exception occured:\n{e}\n{self.query}", kind="error")
            write_summary(msg=f"{self.filename}", kind="failure")
